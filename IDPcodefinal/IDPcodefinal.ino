#include <Wire.h>
#include <Adafruit_MotorShield.h>

#define OPTO_RIGHT A0
#define OPTO_LEFT A1

#define trigPin1 8
#define echoPin1 9

#define trigPin2 10
#define echoPin2 11

long duration1, distance1, distance1prev, change1;
long duration2, distance2, distance2prev, change2;

//// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

//// Select which 'port' M1, M2, M3 or M4
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *CamMotor = AFMS.getMotor(4);
Adafruit_DCMotor *CageMotor = AFMS.getMotor(3);

int leftreading, rightreading = 0; // inititalise the optoswitch readings
int avedistance, walldistance;
int threshold = 600; // set the threshold for the optoswitches reading black or white
int previous_state = 10; // set the threshold for the optoswitches reading black or white
int counter = 0;
int robots, turns = 0;
int stepchange = 20;
int timeinit, timechange = 0;

bool whiteleft, whiteright, righttrig, lefttrig = false;

long duration;
int distance, ultradistance;

void setup() {

  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);

  Serial.begin(9600);

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
}


//Define fowards, left, right, stop

void forwards() {
  RightMotor->setSpeed(100);
  RightMotor->run(FORWARD);
  LeftMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
//  Serial.println("forwards");
}

void backwards() {
  RightMotor->setSpeed(60);
  RightMotor->run(BACKWARD);
  LeftMotor->setSpeed(60);
  LeftMotor->run(BACKWARD);
//  Serial.println("BACKWARDS");
}

void turnonthespot() {
  RightMotor->setSpeed(100);
  RightMotor->run(FORWARD);
  LeftMotor->setSpeed(100);
  LeftMotor->run(BACKWARD);
  delay(3000);//SUBJECT TO CALIBRATION
  stopping();
}

void turnonthespotright() {
  RightMotor->setSpeed(100);
  RightMotor->run(BACKWARD);
  LeftMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  delay(3000);//SUBJECT TO CALIBRATION
  stopping();
}

void forwardstime(int x) {
  RightMotor->setSpeed(200);
  RightMotor->run(FORWARD);
  LeftMotor->setSpeed(200);
  LeftMotor->run(FORWARD);
  delay(x);
  stopping();
}

void backwardstime(int x) {
  RightMotor->setSpeed(200);
  RightMotor->run(BACKWARD);
  LeftMotor->setSpeed(200);
  LeftMotor->run(BACKWARD);
  delay(x);
  stopping();
}

void left() {
  RightMotor->setSpeed(100);
  RightMotor->run(FORWARD);
  LeftMotor->setSpeed(20);
  LeftMotor->run(RELEASE);
  Serial.println("LEFT");
}

void right() {
  
  RightMotor->setSpeed(20);
  RightMotor->run(RELEASE);
  LeftMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  Serial.println("RIGHT");
}
void stopping() {
  RightMotor->run(RELEASE);
  LeftMotor->run(RELEASE);
}

void dropramp() {
  CamMotor->setSpeed(250);
  CamMotor->run(BACKWARD);
  delay(700);
  CamMotor->run(RELEASE);
}

void raiseramp() {
  CamMotor->setSpeed(250);
  CamMotor->run(BACKWARD);
  delay(1100);
  CamMotor->run(RELEASE);
}

void raisecage() {
  CageMotor->setSpeed(250);
  CageMotor->run(BACKWARD);
  delay(3000);
  CageMotor->run(RELEASE);
}

void dropcage() {
  CageMotor->setSpeed(250);
  CageMotor->run(FORWARD);
  delay(2500);
  CageMotor->run(RELEASE);
}

void turnright(int x) {
  RightMotor->setSpeed(80);
  RightMotor->run(BACKWARD);
  LeftMotor->setSpeed(80);
  LeftMotor->run(FORWARD);
  delay(x);
  stopping();
}


void turnleft(int x) {
  RightMotor->setSpeed(80);
  RightMotor->run(FORWARD);
  LeftMotor->setSpeed(80);
  LeftMotor->run(BACKWARD);
  delay(x);
  stopping();
}

long ultrasonic() {

  digitalWrite(trigPin1, LOW);  // Added this line
  delayMicroseconds(2); // Added this line

  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin1, LOW);
  duration1 = pulseIn(echoPin1, HIGH);
  distance1 = (duration1/2) / 29.1;
  delay(50);

//   if (distance1 >= 300 || distance1 <= 0){
//    Serial.println("Out of range");
//  }
//  else {
//    Serial.print ( "Sensor1  ");
//    Serial.print ( distance1);
//    Serial.println("cm");
//  }
  

  digitalWrite(trigPin2, LOW);  // Added this line
  delayMicroseconds(2); // Added this line

  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin2, LOW);
  duration2 = pulseIn(echoPin2, HIGH);
  distance2 = (duration2/2) / 29.1;
  delay(50);

  return distance1, distance2;

//   if (distance2 >= 500 || distance2 <= 0){
//    Serial.println("Out of range");
//  }
//  else {
//    Serial.print("Sensor2  ");
//    Serial.print(distance2);
//    Serial.println("cm");
//  }

}

void initial_line_follow() {

//  Serial.println("PASSINTOINITLF");

  rightreading = analogRead(OPTO_RIGHT);
  
  leftreading = analogRead(OPTO_LEFT);

  if (rightreading > threshold) {
    whiteright = true;
  }
  else {
    whiteright = false;
  }
//  Serial.println("rightreading");
//  Serial.println(whiteright);

  if (leftreading > threshold) {
    whiteleft = true;
  }
  else {
    whiteleft = false;
  }
//  Serial.println("left reading");
//  Serial.println(whiteleft);
  Serial.println("counter ");
  Serial.println(counter);
//  Serial.println("time ");
//  Serial.println(timechange);


  if (whiteleft == false && whiteright == false ) { //&& previous_state != 0
    if (previous_state == 0) {
      return;
    }
    forwards(); //on the white line hopefully
    previous_state = 0;
//    Serial.println("forwards");
  }
  else if (whiteleft == false && whiteright == true) {
    if (previous_state == 1) {
      return;
    }
    right(); // right of line
    previous_state = 1;
  }
  else if (whiteleft == true && whiteright == false) {
    if (previous_state == 2) {
      return;
    }
    left(); // left of line
    previous_state = 2;
  }
  else if (whiteleft == true && whiteright == true) {
    if (previous_state == 3) {
      return;
    }
    timechange = millis() - timeinit;
    timeinit = millis();
    if (timechange < 4000) {
      return;
    }
    counter++;
    previous_state = 3;
//
//    if (counter = 2) {
//        RightMotor->setSpeed(20);
//        RightMotor->run(FORWARD);
//        LeftMotor->setSpeed(80);
//        LeftMotor->run(FORWARD); // middle
//    }

//    if (counter = 3) {
//      stopping();
//      counter++; // both sensors see white so end of track hopefully
//    }
    
    forwards();
  }
}

void line_follow_back(bool goinghome) {

  rightreading = analogRead(OPTO_RIGHT);
  leftreading = analogRead(OPTO_LEFT);

  if (rightreading > threshold) {
    whiteright = true;
  }
  else {
    whiteright = false;
  }
  //Serial.println(rightreading);

  if (leftreading > threshold) {
    whiteleft = true;
  }
  else {
    whiteleft = false;
  }
  //Serial.println(leftreading);


  if (whiteleft == false && whiteright == false ) { //&& previous_state != 0
//    if (previous_state == 0) {
//      return;
//    }
    forwards(); //on the white line hopefully
    previous_state = 0;
    //    Serial.println("forwards");
  }
  else if (whiteleft == true && whiteright == false) {
    if (previous_state == 1) {
      return;
    }
    left(); // right of line
    previous_state = 1;
  }
  else if (whiteright == true && whiteleft == false) {
    if (previous_state == 2) {
      return;
    }
    right(); // left of line
    previous_state = 2;
  }
  else if (whiteleft == true && whiteright == true) {
    if (previous_state == 3) {
      return;
    }
    timechange = millis() - timeinit;
    timeinit = millis();
    if (timechange < 4000) {
      return;
    }
    counter++;
    previous_state = 3;
    forwards();

    if (counter = 3 && goinghome) {
      turnleft(400); //at the junction turn right
      forwardstime(100);
    }
    else if (counter = 3 && !goinghome) {
      turnright(400); //at the junction turn right
      forwardstime(100);
    }
    else if (counter > 3) {
      forwardstime(100);
      stopping();
      drop();
      backwardstime(500);
      turnonthespot();
      turnonthespot();
  }
  }
}

void istherearobot() {
  turnonthespot();
  turnleft(500);
  
  distance1, distance2 = ultrasonic();
//  walldistance = min(distance1,distance2); //use left sensor for this
  righttrig = false;
  lefttrig = false;
  while(righttrig != true and lefttrig != true){
//    if (turns = 20){
//      turnonthespot();
//      turnonthespot();
//      stepchange = stepchange - 5;
//      turns = 0;
//    }
    distance1prev = distance1;
    distance2prev = distance2;

//    Serial.print("Sensor1  ");
//    Serial.print(distance1);
//    Serial.println("cm");
//  
//    Serial.print("Sensor2 ");
//    Serial.print(distance2);
//    Serial.println("cm");

    distance1, distance2 = ultrasonic();
    change1 = distance1prev - distance1;
    change2 = distance2prev - distance2;
//    Serial.print("Change1 ");
//    Serial.print(change1);
//    Serial.println("cm");
//
//    Serial.print("Change2 ");
//    Serial.print(change2);
//    Serial.println("cm");
    
    if(change1 > stepchange){
      righttrig = true;
    }
    if(change2 > stepchange){
      righttrig = true;
    }
    turnright(50);
//    turns++;

  }
  turnright(400); // to correct for the cone effect
  go_to_robot(min(distance1, distance2));
}

void go_to_robot(int travel){
//  distance1, distance2 = ultrasonic();
//  avedistance = 0.5*(distance1 + distance2);
//  while(min(distance1, distance2)>5){
//    distance1, distance2 = ultrasonic();
//    forwards();
//  }
  raisecage();
  forwardstime(travel*60); //distance in cm * time per cm
  stopping();
  grab(travel);
}

void grab(int travel){
  dropramp();
  forwardstime(150);
  dropcage();
  delay(1000);
  raiseramp();
  
  previous_state = 10;
  counter = 0;
  backwardstime(travel*30);
  turnonthespot();
  turnonthespot();
  turnleft(100);
  previous_state = 10;
  line_follow_back(false);
  


}

void linefollow(){
  counter = 0;
    while (counter < 2){
    initial_line_follow();
  }
}

//void grabtest(){
//  dropramp();
//  raisecage();
//  forwardstime(1500);
//  dropcage();
//  raiseramp();
//  previous_state = 10;
//  counter = 0;
//  
//  backwardstime(travel*40);
//  turnonthespot();
//  turnonthespot();
//  line_follow_back(false);
//
//}

void drop(){
  dropramp();
  raisecage();
  backwardstime(100);
  dropcage();
  raiseramp();
  robots++;
  turnonthespot();
  turnonthespot();
  counter = 0;
  linefollow();
}

void loop() {
  // put your main code here, to run repeatedly:

//  distance1, distance2 = ultrasonic();
//  
//  Serial.print("Sensor2  ");
//  Serial.print(distance2);
//  Serial.println("cm");
//  
//  Serial.print("Sensor1  ");
//  Serial.print(distance2);
//  Serial.println("cm");

  //  turnleft(2000);
  //  delay(2000);

 
  timeinit = millis();
  linefollow();
  stopping();
  
//  delay(1000);



//left();

    istherearobot();
  
//counter = 0;
  line_follow_back(false);
  linefollow();
//
//  delay(1000);
//  turnonthespot();
//  delay(2000);
//  turnonthespotright();
//turnright(200);
//right();

//  forwards();

//    left();
//  forwardstime(5000);
//  delay(10000);
  
//  dropramp();
//  raisecage();
//delay(1000);
////grabtest();
////delay(10000);
//  raisecage();
//delay(1500);;
//  dropcage();
//  
//  raiseramp();
  
//  raisecage();

//dropramp();
//delay(2000);
//raiseramp();
//delay(2000);



//
//
//  Serial.println("leftreading ");
//  Serial.println(leftreading);
//  Serial.println("rightreading ");
//  Serial.println(rightreading);
//  Serial.println("counter ");
//  Serial.println(counter);

  //  Serial.println(distance);



}
