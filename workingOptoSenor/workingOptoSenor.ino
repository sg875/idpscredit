#define OPTO_PIN A

int reading = 0;
int threshold = 80;
bool white = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(OPTO_PIN, INPUT);

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  reading = analogRead(A0);
  if(reading > threshold){
    white = false;
    }
   else{
    white = true;
    } 
  Serial.println(reading);
}
