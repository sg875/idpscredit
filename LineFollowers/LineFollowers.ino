#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *RightMotor = AFMS.getMotor(1);
// You can also make another motor on port M2
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(2);

#define OPTO_RIGHT A0
#define OPTO_LEFT A1

int leftreading, rightreading = 0;
int threshold = 35;
bool whiteleft, whiteright = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(OPTO_RIGHT, INPUT);
  pinMode(OPTO_LEFT, INPUT);
  Serial.begin(9600);
  
  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  LeftMotor->setSpeed(150);
  RightMotor->setSpeed(150);
}


void forwards() {
  RightMotor->run(FORWARD);
  LeftMotor->run(FORWARD);
}

void left() {
  RightMotor->run(FORWARD);
  LeftMotor->run(RELEASE);
}

void right() {
  RightMotor->run(RELEASE);
  LeftMotor->run(FORWARD);
}


void loop() {
  // put your main code here, to run repeatedly:
  
  rightreading = analogRead(A0);
  if(rightreading > threshold){
    whiteright = false;
    }
   else {
    whiteright = true;
    } 

   
  leftreading = analogRead(A1);
  if(leftreading > threshold){
    whiteleft = false;
    }
   else {
    whiteleft = true;
    } 
  //forwards();

  Serial.println(rightreading);
}
