#define trigPin 10
#define echoPin 9

long duration;
int distance;

void setup() {
  // put your setup code here, to run once:
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(trigPin, HIGH);   // start sending US pulse
  delayMicroseconds(10);         // wait for 10 ms
  digitalWrite(trigPin, LOW);    // stop sending US pulse
  duration = pulseIn(echoPin, HIGH);
  distance = duration * 0.034/2;
  Serial.println(distance);
}
