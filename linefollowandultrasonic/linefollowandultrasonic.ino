#include <Wire.h>
#include <Adafruit_MotorShield.h>

#define OPTO_RIGHT A1
#define OPTO_LEFT A0
#define trigPin 10
#define echoPin 9

//// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
//// Or, create it with a different I2C address (say for stacking)
//// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

//// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *RightMotor = AFMS.getMotor(1);
//// You can also make another motor on port M2
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(2);

int leftreading, rightreading = 0; // inititalise the optoswitch readings

int threshold = 35; // set the threshold for the optoswitches reading black or white
int previous_state = 0; // set the threshold for the optoswitches reading black or white

bool whiteleft, whiteright = false;

long duration;
int distance;

void setup() {

  pinMode(OPTO_RIGHT, INPUT);
  pinMode(OPTO_LEFT, INPUT);
  Serial.begin(9600);

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  LeftMotor->setSpeed(50);
  RightMotor->setSpeed(50);

}



void forwards() {
  RightMotor->run(FORWARD);
  LeftMotor->run(FORWARD);
  }

void left() {
  RightMotor->run(FORWARD);
  LeftMotor->run(RELEASE);
  }

void right() {
  RightMotor->run(RELEASE);
  LeftMotor->run(FORWARD);
  }
void stopping() {
  RightMotor->run(RELEASE);
  LeftMotor->run(RELEASE);
  }

void loop() {
  // put your main code here, to run repeatedly:

  digitalWrite(trigPin, HIGH);   // start sending US pulse
  delayMicroseconds(10);         // wait for 10 ms
  digitalWrite(trigPin, LOW);    // stop sending US pulse
  duration = pulseIn(echoPin, HIGH);
  distance = duration * 0.034/2;
//  Serial.println(distance);
  
  rightreading = analogRead(OPTO_RIGHT);
  if(rightreading > threshold){
    whiteright = true;
    }
    
    else {
    whiteright = false;
    } 
  //Serial.println(rightreading);

   
  leftreading = analogRead(OPTO_LEFT);
  if(leftreading > threshold){
    whiteleft = true;
    }
   else {
    whiteleft = false;
    }
   Serial.println("leftreading ");
   Serial.println(leftreading);
   Serial.println("rightreading ");
   Serial.println(rightreading);

   if (whiteleft == false && whiteright == false && previous_state != 0){
    forwards(); //on the white line hopefully
    previous_state = 0;
    Serial.println("forwards");
   }
   else if (whiteleft == false && whiteright == true && previous_state != 1){
    left(); // right of line
    previous_state = 1;
   }
   else if (whiteright== true && whiteleft == false && previous_state != 2){
    right(); // left of line
    previous_state = 2;
   }
   else {
    if (previous_state != 3){
      counter++;
    }
    previous_state = 3;
    stopping(); // both sensors see white so end of track hopefully
    previous
   }
   
}
